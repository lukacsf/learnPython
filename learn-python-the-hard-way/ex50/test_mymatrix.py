import unittest
import mymatrix.mymatrix as m

class MatrixTests(unittest.TestCase):
    entries = [[0, 1, 0], [1, 0, 1], [0, 0, 1]]
    mat = m.Matrix(entries)
    
    def test_initial_dim_checks(self):
        """
        Tests whether the initial dimension checks are performed
        """
        malformed_entries = [[0, 1, 0], [1, 0, 1], [0, 0]]
        self.assertRaises(m.MalformedMatrix, m.Matrix, malformed_entries)

    def test_class_check(self):
        """
        Tests whether the resulting object is of the correct class
        """
        self.assertIsInstance(self.mat, m.Matrix)

    def test_add_dim_check(self):
        """
        Tests whether the dimension check before addition is performed
        """
        entries1 = [[1, 0, 0],
                    [0, 1, 0],
                    [0, 0, 1]]
        mat1 = m.Matrix(entries1)

        entries2 = [[1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]]
        mat2 = m.Matrix(entries2)

        self.assertRaises(m.MatrixDimensionError, mat1.__add__, mat2)
        

    def test_add_1(self):
        """
        Tests whether addition of matrices is performed correctly
        """
        entries1 = [[1, 2, 3],
                    [4, 5, 6]]

        entries2 = [[6, 5, 4],
                    [3, 2, 1]]

        result = [[7, 7, 7],
                  [7, 7, 7]]

        test_matrix = m.Matrix(entries1) + m.Matrix(entries2)

        self.assertEqual(test_matrix, m.Matrix(result))

    def test_mul_dim_check(self):
        """
        Tests whether the dimension check before multiplication is performed
        """
        entries1 = [[1, 0, 3],
                    [0, 1, 7]]
        mat1 = m.Matrix(entries1)

        entries2 = [[1, 0, 0],
                    [3, 4, 5]]
        mat2 = m.Matrix(entries2)

        self.assertRaises(m.MatrixDimensionError, mat1.__mul__, mat2)

    def test_mul_1(self):
        """
        Tests whether multiplication of matrices is performed correctly
        """
        entries1 = [[1, 2, 3],
                    [4, 5, 6]]
        mat1 = m.Matrix(entries1)

        entries2 = [[1, 2],
                    [3, 4],
                    [5, 6]]
        mat2 = m.Matrix(entries2)

        result = [[22, 28],
                  [49, 64]]

        self.assertEqual(mat1 * mat2, m.Matrix(result))


if __name__ == "__main__":
    unittest.main()

class MalformedMatrix(Exception):
    pass

class MatrixDimensionError(Exception):
    pass


class Matrix(object):
    def __init__(self, entries):
        if not entries:
            raise MalformedMatrix("This matrix is empty.")
        
        else:
            self.__dim_rows = len(entries)
            self.__dim_cols = len(entries[0])
            
            for rows in entries:
                if len(rows) != self.__dim_cols:
                    raise MalformedMatrix("The lengths of the rows do not match.")
                
            self.__entries = entries

    def get_entries(self):
        return self.__entries

    def get_dim_rows(self):
        return self.__dim_rows

    def get_dim_cols(self):
        return self.__dim_cols

    def __eq__(self, other):
        return self.__entries == other.get_entries()

    def __add__(self, other):
        if self.__dim_rows == other.get_dim_rows() and self.__dim_cols == other.get_dim_cols():
            sum_matrix = []
            
            for rowA, rowB in zip(self.__entries, other.get_entries()):
                sum_row = []
                
                for entryA, entryB in zip(rowA, rowB):
                    sum_row.append(entryA + entryB)
                    
                sum_matrix.append(sum_row)
                
            return Matrix(sum_matrix)
        
        else:
            raise MatrixDimensionError("The dimensions of the matrices have to match.")

    def transpose(self):
        transposed_entries = []
        
        for i in range(len(self.__entries[0])):
            transposed_row = []
            
            for row in self.__entries:
                transposed_row.append(row[i])
                
            transposed_entries.append(transposed_row)
            
        self.__entries = transposed_entries
        self.__dim_rows, self.__dim_cols = self.__dim_cols, self.__dim_rows
        
        return self

    def __mul__(self, other):
        if self.__dim_cols == other.get_dim_rows():
            
            prod_matrix = []
            other.transpose()
            
            for row in self.__entries:
                
                prod_row = []
                
                for col in other.get_entries():
                    prod_entry = sum([x * y for x, y in zip(row, col)])
                    prod_row.append(prod_entry)
                    
                prod_matrix.append(prod_row)
                
            return Matrix(prod_matrix)
        
        else:
            raise MatrixDimensionError("These matrices cannot be multiplied due to their dimensions.")

    def __repr__(self):
        str_repr = ""
        
        for row in self.__entries:
            str_repr += (str(row) + '\n')
            
        return str_repr

    def scalar_mult(self, scalar):
        self.__entries = [ list(map((lambda x: scalar * x), row)) for row in self.__entries ]
        return self

class Parent(object):

    def override(self):
        print("PARENT override()")

    def implicit(self):
        print("PARENT implicit()")

    def altered(self):
        print("PARENT altered()")

class Child(Parent):

    def override(self):
        print("CHILD override()")

    def altered(self):
        print("CHILD BEFORE altered()")
        super(Child, self).altered()
        print("CHILD AFTER altered()")

dad = Parent()
son = Child()

print("-"*10)

dad.implicit()
son.implicit()

print("-"*10)

dad.override()
son.override()

print("-"*10)

dad.altered()
son.altered()

print("-"*10)

from ex48 import Lexicon

class ParserError(Exception):
    pass

class Sentence(object):

    def __init__(self, subject, verb, obj):
        self.subject = subject[1]
        self.verb = verb[1]
        self.obj = obj[1]

def peek(word_list):
    if word_list:
        return word_list[0][0]
    else:
        return None

def match(word_list, expecting):
    if word_list:
        word = word_list.pop(0)

        if word[0] == expecting:
            return word
        else:
            return None
    else:
        return None

def skip(word_list, word_type):
    while peek(word_list) == word_type:
        match(word_list, word_type)
        
def parse_verb(word_list):
    skip(word_list, 'stop word')

    if peek(word_list) == 'verb':
        return match(word_list, 'verb')
    else:
        raise ParserError("Expected verb next.")

def parse_object(word_list):
    skip(word_list, 'stop word')
    next_word = peek(word_list)

    if next_word == 'noun':
        return match(word_list, 'noun')
    elif next_word == 'direction':
        return match(word_list, 'direction')
    else:
        raise ParserError("Expected a noun or a direction next.")

def parse_subject(word_list):
    skip(word_list, 'stop word')
    next_word = peek(word_list)

    if next_word == 'noun':
        return match(word_list, 'noun')
    elif next_word == 'verb':
        return ('noun', 'player')
    else:
        raise ParserError("Expected a verb next.")

def parse_sentence(word_list):
    subj = parse_subject(word_list)
    verb = parse_verb(word_list)
    obj = parse_object(word_list)

    return Sentence(subj, verb, obj)

my_user_inputs = ["north south east",
               "go kill eat",
               "the in of",
               "bear princess",
               "3 91234",
               "ASDFASDFASDF",
               "bear IAS princess"
               ]

user_inputs = ["run to the door",
               "eat the bear"]

lexicon = Lexicon()

for user_input in user_inputs:
    sentence = parse_sentence(lexicon.scan(user_input))
    print(sentence.subject)
    print(sentence.verb)
    print(sentence.obj)

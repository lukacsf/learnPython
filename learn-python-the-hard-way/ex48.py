class Lexicon(object):
    direction = ["north", "south", "east", "west", "south", "up", "down", "left", "right", "back"]
    verb = ["go", "stop", "kill", "eat", "run"]
    stop_word = ["the", "in", "of", "from", "at", "it", "to"]
    noun = ["door", "bear", "princess", "cabinet"]

    def scan(self, user_input):
        words = user_input.split()
        sentence = []
        for word in words:
            sentence.append(check(self, word))
        return sentence

def convert_number(s):
    try:
        return int(s)
    except ValueError:
        return None


def check(lexicon, word):
    number_p = convert_number(word)
    if word in lexicon.direction:
        return ("direction", word)
    elif word in lexicon.verb:
        return ("verb", word)
    elif word in lexicon.stop_word:
        return ("stop word", word)
    elif word in lexicon.noun:
        return ("noun", word)
    elif number_p != None:
        return ("number", number_p)
    else:
        return ("error", word)

lexicon = Lexicon()

user_inputs = ["north south east",
               "go kill eat",
               "the in of",
               "bear princess",
               "3 91234",
               "ASDFASDFASDF",
               "bear IAS princess"
               ]

# for user_input in user_inputs:
#    print(lexicon.scan(user_input))

my_tuple = (1,2,3)

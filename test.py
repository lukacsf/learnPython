import time

class Node(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def insert(self, value):
        if value < self.value:
            if self.left == None:
                self.left = Node(value)
            else:
                self.left.insert(value)
        else:
            if self.right == None:
                self.right = Node(value)
            else:
                self.right.insert(value)

      	  
def from_list(list):
    if list == []:
        return None
    else:
        my_tree = Node(list[0])
        for element in list[1:]:
            my_tree.insert(element)
        return my_tree

def to_list(tree):
    if tree == None:
        return []
    else:
        return to_list(tree.left) + [tree.value] + to_list(tree.right)

# I couldn't resist...
def fmap_tree(f, tree):
    if tree == None:
        return None
    else:
        return Node(f(tree.value), fmap_tree(f, tree.left), fmap_tree(f, tree.right))

my_list = [1, 5, 3, 8, 6, 9, 100]
my_sorted_squared_list = to_list(fmap_tree((lambda x: x**2), from_list(my_list)))

print(my_list)
print(my_sorted_squared_list)

def inorder(tree):
    if tree:
        for x in inorder(tree.left):
            yield x

        yield tree.value

        for y in inorder(tree.right):
            yield y

hopefully_sorted_list = [x for x in inorder(from_list(my_list))]            
print(hopefully_sorted_list)

class File(object):
    def __init__(self, filename, mode):
        self.filename = filename
        try:
            self.file = open(filename, mode)
        except FileNotFoundError:
            with open(filename, 'w') as new_file:
                print("This file does not exist, creating it now...")
                new_file.write("This file did not exist, created it just now.\n")
            self.file = open(filename, mode)

    def __enter__(self):
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()
        if exc_type is Exception:
            print("I don't know what this is...")
            print("But let's continue, I guess...")
            return True

with File("valami.txt", 'r') as my_file:
    print(my_file.readline())


def timer(f):
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        return_values = f(*args, **kwargs)
        print(time.perf_counter() - start_time)
        return return_values

    return wrapper

@timer
def sleeping():
    time.sleep(2)

# sleeping()

def bin_search(array, element):
    print(array)
    index, middle = list(enumerate(array))[len(array) // 2]
    if element == middle:
        return index
    elif element < middle:
        return bin_search(array[:index], element)
    elif element > middle:
        return bin_search(array[index:], element)
    else:
        raise ValueError("The required element is not in the list")


my_array = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37]
#print(bin_search(my_array, 3))

def get_parent(heap, index):
    if index == 0:
        raise IndexError("This is the root, it has no parent")
    elif index % 2 == 0:
        parent_index = (index - 2) // 2
        return parent_index, heap[parent_index]
    elif index % 2 == 1:
        parent_index = (index - 1) // 2
        return parent_index, heap[parent_index]

def insert_min_heap(heap, element):
    index = len(heap)
    heap.append(element)

    while True:
        try:
            parent_index, parent_value = get_parent(heap, index)
            if element < parent_value:
                heap[index], heap[parent_index] = parent_value, element
                index = parent_index
            else:
                break
        except IndexError:
            break

def get_left_child(heap, index):
    left_index = 2 * index + 1
    return left_index, heap[left_index]

def get_right_child(heap, index):
    right_index = 2 * index + 2
    return right_index, heap[right_index]

def delete_min_heap(heap, index):
    replacement = heap[-1]
    heap[index] = replacement
    heap.pop()

    while True:
        try:

            # We want to swap with the smaller of the children
            left_index, left_value = get_left_child(heap, index)
            right_index, right_value = get_right_child(heap, index)
            if left_value < right_value:
                child_index, child_value = left_index, left_value
            else:
                child_index, child_value = right_index, right_value

            # Swapping...
            if child_value < replacement:
                heap[index], heap[child_index] = child_value, replacement
                index = child_index
            else:
                break

        except IndexError:
            break

def build_heap(list):
    heap = []
    for element in list:
        insert_min_heap(heap, element)

    return heap

class TrieNode(object):
    def __init__(self, value, children=None):
        self.val = value
        self.children = children

    def get_children_value(self):
        children_value = []
        try:
            for child in self.children:
                children_value.append(child.val)

            return children_value
        
        except TypeError:
            return []



"""
How to insert a word into a trie?
1. Take the first letter and the trie.
2. Check is there is a node with the same letter in trie.children
3a If there is, then there is nothing to do, move on to the next letter
3b If there isn not, then create a node with it and append it to the list of children
4a Take the next letter and the trie starting at the node that holds the previous letter
   and go to point 2.
4b If there are no more letters, return the new trie
"""
def trie_insert(trie, word):
    word += '$'
    current_node = trie
    for letter in word:

        try:
            index = current_node.get_children_value().index(letter)
            current_node = current_node.children[index]
        except ValueError:
            try:
                current_node.children.append(TrieNode(letter))
                current_node = current_node.children[-1]
            except AttributeError:
                current_node.children = [TrieNode(letter)]
                current_node = current_node.children[0]
            
        
def build_trie(words):
    trie = TrieNode('$')
    for word in words:
        trie_insert(trie, word)

    return trie

def print_trie(trie):
    children = ""
    try:
        for child in trie.children:
            children += print_trie(child)
    except TypeError:
        pass

    return f"{trie.val}: [{children}]"

def trie_search(trie, word):
    word += '$'
    current_node = trie

    for letter in word:
        try:
            index = current_node.get_children_value().index(letter)
            current_node = current_node.children[index]

        except ValueError:
            return False

    return True

"""
Find all paths in a tree
1. Take the root and an empty string
2. Loop through the children of the root and for each child:
- if its value is '$', than that's the end of a word, so append string to words
- otherwise add its value to the string, and continue the search with the longer string.
"""
def find_all_words(trie):
    
    def scan(trie, words, string):
        for child in trie.children:
            modified_string = string[:]
            if child.val == '$':
                words.append(string)
            else:
                modified_string += child.val
                words = scan(child, words, modified_string)

        return words
            
    return scan(trie, [], "")
    

def with_prefix(trie, prefix):

    def last_common_node(trie, prefix):
        current_node = trie
        for letter in prefix:
            try:
                index = current_node.get_children_value().index(letter)
                current_node = current_node.children[index]
            except ValueError:
                return None

        return current_node

    lcn = last_common_node(trie, prefix)
    suffixes = find_all_words(lcn)

    words_with_prefix = []

    for suffix in suffixes:
        words_with_prefix.append(prefix + suffix)

    return words_with_prefix


words = "kés villa villő világ villám".split()
trie = build_trie(words)
print(print_trie(trie))
print(trie_search(trie, "kés"))

print(with_prefix(trie, "vil"))


def quicksort(l):
    length = len(l)

    if length < 2:
        return l
    
    else:
        pivot = l[-1]
        left = 0
        right = length - 2

        while left < right:

            if l[left] > pivot and l[right] <= pivot:
                l[left], l[right] = l[right], l[left]

            if l[left] < pivot:
                left += 1

            if l[right] > pivot:
                right -= 1
                        
        half = length // 2
        smaller = quicksort(l[:half])
        bigger = quicksort(l[half:])
        
        return smaller + bigger

print(quicksort([1, 4, 2, 3, 9, 5, 8, 7, 6]))


# Function to find the partition position
def partition(array, low, high):
 
    # choose the rightmost element as pivot
    pivot = array[high]
 
    # pointer for greater element
    i = low - 1
 
    # traverse through all elements
    # compare each element with pivot
    for j in range(low, high):
        if array[j] <= pivot:
 
            # If element smaller than pivot is found
            # swap it with the greater element pointed by i
            i = i + 1
 
            # Swapping element at i with element at j
            (array[i], array[j]) = (array[j], array[i])

    # Swap the pivot element with the greater element specified by i
    (array[i + 1], array[high]) = (array[high], array[i + 1])
 
    # Return the position from where partition is done
    return i + 1
 
# function to perform quicksort
 
 
def quickSort(array, low, high):
    if low < high:
 
        # Find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = partition(array, low, high)
 
        # Recursive call on the left of pivot
        quickSort(array, low, pi - 1)
 
        # Recursive call on the right of pivot
        quickSort(array, pi + 1, high)
 
 
data = [1, 7, 4, 1, 10, 9, -2]
print("Unsorted Array")
print(data)

size = len(data)
 
quickSort(data, 0, size - 1)
 
print('Sorted Array in Ascending Order:')
print(data)

class Node(object):
    def __init__(self, value, next=None):
        self.val = value
        self.next = next

class LinkedList(object):
    def __init__(self, head):
        self.head = head

    def insert(self, value):
        current_node = self.head
        while True:
            if current_node.next is None:
                current_node.next = Node(value)
                break
            else:
                current_node = current_node.next

    def __repr__(self):
        current_node = self.head
        string = "[["
        while current_node:
            string += str(current_node.val) + ", "
            current_node = current_node.next

        return string + "\b\b]]"

def from_list(l):
    if l:
        first_element = l.pop(0)
        linked_list = LinkedList(Node(first_element))

        for element in l:
            linked_list.insert(element)

        return linked_list

    else:
        return None


class HashTable(object):
    def __init__(self, fn, no_of_indices):
        self.__data = [None] * no_of_indices
        self.hash_fn = fn
        self.index_fn = (lambda x: x)

    def insert(self, key, value):
        hash_value = self.hash_fn(key)
        index = self.index_fn(hash_value)

        try:
            self.__data[index].insert((key, value))
            
        except AttributeError:
            self.__data[index] = Node((key, value))

    
    def retrieve(self, key):
        hash_value = self.hash_fn(key)
        index = self.index_fn(hash_value)

        current_node = self.__data[index]

        while True:
            if current_node is None:
                raise KeyError("This key is not in the hash table")
            else:
                found_key, value = current_node.val
                if key == found_key:
                    return value
                
                else:
                    current_node = current_node.next

                    
def get_hashing_fn(p, m):

    def hashing(key):
        hash = 0
        for index, char in enumerate(key):
            hash += (1 + ord(char) - ord('a')) * (p ** index)

        return hash % m

    return lambda key: hashing(key)
    
hash_fn = get_hashing_fn(31, 10**9 + 7)

hash_table = HashTable(hash_fn, 10**9 + 7)

hash_table.insert("fa", "Géza")



class Solution:
    def twoSum(self, nums, target):
        for index1, num in enumerate(nums):
            if target - num in nums:
                num2 = target - num
                index2 = nums.index(num2)
                if index1 == index2:
                    pass
                else:
                    return [index1, index2]

test = Solution()

print(test.twoSum(range(10001), 19999))

class Solution(object):
    def group_by_num_rows(self, s, num_rows):
        n_blocks = []
        k = 2 * num_rows - 2
        while len(s) >= k:
            n_blocks.append(tuple(s[:k]))
            s = s[k:]

        m = k - len(s)
        last_block = tuple(s + m * ' ')
        n_blocks.append(last_block)

        return n_blocks

    def process_block(self, chars, num_rows):
        block = []
        for k in range(num_rows):
            line = chars[k] + " "
            if k != (num_rows - 1):
                line += (num_rows - k - 2) * "  "
                if k != 0:
                    line += chars[-k] + " "
            # Egy sor hosszának 2n-2 hosszúnek kell lennie
            m = (2 * num_rows -2) - len(line)
            line += m * " "
            block.append(line)

        return block

    def prepare_blocks(self, grouped, num_rows):
        blocks = []
        
        for block in grouped:
            new_block = self.process_block(block, num_rows)
            blocks += [new_block]

        return blocks

    def prepare_string(self, blocks, num_rows):
        string = ""
        for line in range(num_rows):
            for block in blocks:
                string += block[line]
            string += "\n"

        return string

    def convert(self, s, num_rows):
        grouped = self.group_by_num_rows(s, num_rows)
        blocks = self.prepare_blocks(grouped, num_rows)
        formatted_string = self.prepare_string(blocks, num_rows)

        # remove whitespace
        string = ""
        for char in formatted_string:
            if char != ' ' and char != '\n':
                string += char

        return string



test = Solution()
string = "bélajánosgéza"
string2 = "PAYPALISHIRING"
n = 4

lorem_ipsum = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
"""

lorem = ""
for char in lorem_ipsum:
    if char != ' ':
        lorem += char

# print(test.convert(lorem, 3))

print(test.convert(string2, 3))

class Solution:
    def palindrome_p(self, s):
        for index in range(len(s) // 2):
            if not s[index] == s[-index - 1]:
                return False
        return True

    def longest_palindrome_with_center_odd(self, s, center):
        left = center - 1
        right = center + 1
        length = len(s)
        palindrome_found = s[center]
        
        while left >= 0 and right < length:
            if s[left] == s[right]:
                palindrome_found = s[left] + palindrome_found + s[right]
                left -= 1
                right += 1
            else:
                break

        return palindrome_found

    def longest_palindrome_with_center_even(self, s, center):
        """
        It is assumed that s[center] == s[center + 1] and both are valid
        """
        left = center - 1
        right = center + 2
        length = len(s)
        palindrome_found = s[center] + s[center + 1]

        while left >= 0 and right < length:
            if s[left] == s[right]:
                palindrome_found = s[left] + palindrome_found + s[right]
                left -= 1
                right += 1
            else:
                break

        return palindrome_found
            
                
    def longestPalindrome(self, s):
        # naive version
        length = len(s)
        current_longest_palindrome = ""
        for index in range(length):

            # even palindromes
            longest_palindrome_with_center_index_odd = self.longest_palindrome_with_center_odd(s, index)
            if len(longest_palindrome_with_center_index_odd) > len(current_longest_palindrome):
                current_longest_palindrome = longest_palindrome_with_center_index_odd

            # odd palindromes
            try:
                if s[index] == s[index + 1]:
                    longest_palindrome_with_center_index_even = self.longest_palindrome_with_center_even(s, index)
                    if len(longest_palindrome_with_center_index_even) > len(current_longest_palindrome):
                        current_longest_palindrome = longest_palindrome_with_center_index_even
            except IndexError:
                pass

        return current_longest_palindrome

test = Solution()

# print(test.palindrome_p("baab"))

# print(test.longest_palindrome_with_center_even("baab", 1))

string = """
abababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababababa
"""

print(test.longestPalindrome(string))

class Solution:
    def maxArea(self, height):
        def volume(tup):
            x, y = tup
            xi, xh = x
            yi, yh = y
            height = min(xh, yh)
            width = yi - xi
            return height * width

        def remainder(index):
            rem_list = height[index+1:]
            return zip(range(index+1, len(height)), rem_list)

        return max(map((lambda tup: volume(tup)), ((x, y) for x in enumerate(height) for y in remainder(x[0]))))

    def maxArea2(self, height):

        def area(l,r):
            h = min(height[l], height[r])
            w = r - l
            return h * w
        
        l, r, max_area = 0, len(height)-1, 0

        while l < r:
            max_area = max(max_area, area(l, r))
            if height[l] < height[r]:
                l += 1
            else:
                r -= 1

        return max_area
            
test = Solution()

height = [1,8,6,2,5,4,8,3,7]

print(test.maxArea2(range(5000)))


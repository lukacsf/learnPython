# Definition for singly-linked list.
from typing import List


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        return "[[ " + self.show_values() + "\b\b ]]"

    def show_values(self):
        curr = self
        string = str(curr.val) + ", "
        while curr.next:
            curr = curr.next
            string += str(curr.val) + ", "

        return string

def from_list(reg_list = [ 0 ]):
    
    if reg_list == []:
        reg_list = [ 0 ]

    head = ListNode(reg_list[0])
    curr_node = head
    for element in reg_list[1:]:
        curr_node.next = ListNode(element)
        curr_node = curr_node.next

    return head


class Solution:
    def addTwoNumbers(self, l1, l2):

        # sum of the first two digits
        current_sum = l1.val + l2.val

        # initialize remainder for when the sum of
        # two digits is greater then 10
        remainder = 0

        # if the sum of two digits is greater than 10,
        # we plit it into two digits and carry on the remainder
        if current_sum > 9:
            remainder = 1
            current_sum = current_sum % 10

        # we append the first digit to the sum
        my_sum = ListNode(current_sum)

        # we save the head so we can return it
        my_sum_head = my_sum

        # set the next round up
        next1 = l1.next
        next2 = l2.next

        # The idea is to first go through the first list
        # If it is longer than the second, we keep appending 0s
        while next1:

            # If the second list is shorter, we append a 0 to the end
            if not next2:
                next2 = ListNode()

            # we add the current digits AND the remainder
            current_sum = next1.val + next2.val + remainder

            # this has now been incorporated into current_sum,
            # so we should annul it
            remainder = 0

            # check if this produces a remainder
            if current_sum > 9:
                remainder = 1
                current_sum %= 10

            # add the calculated next digit
            my_sum.next = ListNode(current_sum)

            # set up the next round
            my_sum = my_sum.next
            
            next1, next2 = next1.next, next2.next

        # if the second list is longer, we just keep adding its values
        while next2:
            current_sum = next2.val + remainder
            remainder = 0
            
            if current_sum > 9:
                remainder = 1
                current_sum %= 10
            
            my_sum.next = ListNode(current_sum)

            my_sum = my_sum.next
            next2 = next2.next

        if remainder != 0:
            my_sum.next = ListNode(remainder)

        return my_sum_head

test = Solution()

l1 = from_list([2, 4, 9])
l2 = from_list([5, 6, 4, 9])

print(test.addTwoNumbers(l1, l2))

print(f"Expected output {from_list([7, 0, 4, 0, 1])}")


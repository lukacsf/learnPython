class Solution(object):
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    signs = ['+', '-']
    relevant = digits + signs + [' ']

    digit_dict = {'0': 0,
                  '1': 1,
                  '2': 2,
                  '3': 3,
                  '4': 4,
                  '5': 5,
                  '6': 6,
                  '7': 7,
                  '8': 8,
                  '9': 9}

    def process_leading_characters(self, string):

        def peek(string, index):
            try:
                return string[index + 1]
            except IndexError:
                return None

        new_string = string[:]
        for index, char in enumerate(string):
            
            if char not in self.relevant:
                return '0'
            
            elif char == ' ':
                new_string = new_string[1:]
                
            elif char in self.digits:
                break
            
            elif char in self.signs and peek(string, index) not in self.digits:
                return '0'
            
            else:
                pass

        if new_string:
            return new_string
        else:
            return '0'

    def process_sign(self, string):
        sign = string[0]
        if sign == '-':
            return string[1:], False
        elif sign == '+':
            return string[1:], True
        else:
            return string, True

    def process_trailing_characters(self, string):
        num_string = ""
        for char in string:
            if char in self.digits:
                num_string += char
            else:
                break

        return num_string

    def read_number(self, string):
        number = 0
        length = len(string) - 1
        
        for index, char in enumerate(string):
            digit = self.digit_dict[char]
            exponent = length - index

            number += digit * (10 ** exponent)

        return number

    def truncate_to_32_bits(self, num):
        if num < - 2**31:
            return - 2**31
        elif num > 2 ** 31 - 1:
            return 2 ** 31 - 1
        else:
            return num

    def myAtoi(self, string):
        string = self.process_leading_characters(string)
        string, positive = self.process_sign(string)
        string = self.process_trailing_characters(string)
        number = self.read_number(string)

        if not positive:
            number *= -1

        number = self.truncate_to_32_bits(number)

        return number
                
                

test = Solution()

print(test.myAtoi("     -1998-ban születtem"))
print(test.myAtoi("Én     -1998-ban születtem"))
print(test.myAtoi("+"))
print(test.myAtoi("-+42"))
print(test.myAtoi(""))
print(test.myAtoi(" "))

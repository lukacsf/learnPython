class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
     current_node = self
     string = "[["
     while current_node:
         string += str(current_node.val) + ", "
         current_node = current_node.next

     return string + "\b\b]]"

    def insert(self, value):
        current_node = self
        while True:
            if current_node.next is None:
                current_node.next = ListNode(value)
                break
            else:
                current_node = current_node.next


def from_list(l):
    if l:
        first_element = l.pop(0)
        linked_list = ListNode(first_element)

        for element in l:
            linked_list.insert(element)

        return linked_list

    else:
        return None

    
class Solution(object):
    def mergeTwoLists(self, list1, list2):
        if list1 and not list2:
            return list1

        elif not list1 and list2:
            return list2

        elif not list1 and not list2:
            return None

        else:
            e2 = list2.val
            list2 = list2.next
            e1 = list1.val
            list1 = list1.next

        print(e1, e2)
        if e1 < e2:
            sorted = ListNode(e1)
            head = sorted
            if list1:
                e1 = list1.val
                list1 = list1.next
            else:
                sorted.next = list2

        else:
            sorted = ListNode(e2)
            head = sorted
            if list2:
                e2 = list2.val
                list2 = list2.next
            else:
                sorted.next = list1

        while True:
            print(head, e1, e2)
            if e1 < e2:
                sorted.next = ListNode(e1)
                sorted = sorted.next
                if list1:
                    e1 = list1.val
                    list1 = list1.next
                else:
                    sorted.next = ListNode(e2)
                    sorted.next.next = list2
                    break

            else:
                sorted.next = ListNode(e2)
                sorted = sorted.next
                if list1:
                    e2 = list2.val
                    list2 = list2.next
                else:
                    sorted.next = ListNode(e1)
                    sorted.next.next = list1
                    break

        return head
                

test = Solution()                

list1 = from_list([1, 2, 4])
list2 = from_list([1, 3, 4])

print(test.mergeTwoLists(list2, list1))

class Solution:

    def romanToInt(self, s):
        # Initialize return value
        number = 0
        s = list(s)

        # Start from the end of the string
        try:
            last = s.pop()
        except IndexError:
            return number

        # If it ends with I...
        if last == 'I':
            number += 1
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'I':
                    number += 1
                else:
                    break

        if last == 'V':
            number += 5
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'V':
                    number += 5
                elif last == 'I':
                    number -= 1
                else:
                    break

        if last == 'X':
            number += 10
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'X':
                    number += 10
                elif last == 'I':
                    number -= 1
                else:
                    break

        if last == 'L':
            number += 50
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'L':
                    number += 50
                elif last == 'X':
                    number -= 10
                else:
                    break

        if last == 'C':
            number += 100
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'C':
                    number += 100
                elif last == 'X':
                    number -= 10
                else:
                    break

        if last == 'D':
            number += 500
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'D':
                    number += 500
                elif last == 'C':
                    number -= 100
                else:
                    break

        if last == 'M':
            number += 1000
            while True:
                try:
                    last = s.pop()
                except IndexError:
                    return number
                if last == 'M':
                    number += 1000
                elif last == 'C':
                    number -= 100
                else:
                    break

        return number

    def romanToIntNice(self, s):
        values = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000
            }
        number = 0
        s = s.replace("IV", "IIII").replace("IX", "VIIII")
        s = s.replace("XL", "XXXX").replace("XC", "LXXXX")
        s = s.replace("CD", "CCCC").replace("CM", "DCCCC")
        for char in s:
            number += values[char]
        return number


sol = Solution()
print(sol.romanToInt('MCMXCIV'))

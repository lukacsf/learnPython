#+TITLE: Python Cookbook
#+AUTHOR: Ferenc Lukács
#+LANGUAGE: en

* Files
Python is capable of interacting with the files stored on the computer. In this section the way text files are handled is presented.

Files can be accessed using the =open()= function. It supports multiple modes, the more important of which are:
+ ='r'=: read
+ ='w'=: write (the original contents of the file will be overwritten
+ ='a'=: append

#+BEGIN_SRC python
  file = open(filename, mode)
#+END_SRC
** Reading Files

Files can be operated on on a per-character basis.
#+BEGIN_SRC python
  file = open("filename.txt", 'r')

  # This prints the whole file
  print(file.read())

  # This prints the first 5 characters
  print(file(read(5))
#+END_SRC

However, most of the time a per-line approach is more useful
#+BEGIN_SRC python
  file2 = open("filename.txt", 'r')

  lines = file2.readlines() # returns a list of all lines in the file

  line = file2.readline() # reads the next line from the file
  while line:
      print(line)
      line = file2.readline()
#+END_SRC

There are also some useful methods for processing the text. The =strip()= method removes whitespace from the beginning and end of the line, while =split()= splits the line into a list of words.
#+BEGIN_SRC python
  file2.seek(0)
  
  line = file2.readline()
  line.strip()
  line.split()

  for word in line:
      print(word)
#+END_SRC

Note that in the previous block every line of the file has already been read. Hence, we need to 'rewind' to the beginning of the file to be able to read lines. To do this, the =seek()= method is used.
** Writing Files

In this mode the original contents of the opened file are overwritten. If the given file does not yet exist, python will create it.
#+BEGIN_SRC python
  new_file = open("my_new_file.txt", 'w')

  file.write("This is some text I wish to write to this file")
  file.write("Writing things into files is fun")

  file.close()
#+END_SRC

There are two things to note here. One, the =write()= method writes *lines* into the file and two, the file has to be closed once the writing porcess finished.

If all the writing happens inside a single block of code, the =with()= function might be used. In that case, explicitly closing the file is not necessary.

#+BEGIN_SRC python
  with open("filename.txt", 'w') as file:
      f.write("This is so much simpler!")
#+END_SRC
** Appending to Files

This works similarly to write mode, but in this case the original content of the file is preserved, new data is merely /appended/ to the file.
#+BEGIN_SRC python
  file3 = open("filename.txt" 'a')

  file3.write("This will not delete anything.")

  file3.close()
#+END_SRC

The =with()= function might also be used in this mode.
* OOP
** Classes
Classes can be defined using the =class= keyword. If there is no inheritance specified, the =object= keyword may be used.

The =__init__= function is the one called whenever the class is instantiated. It also specifies what arguments are required for instantiating an object of the class.

In the block under the class declaration we can define further methods and data that belongs to the class.

#+BEGIN_SRC python
  class MyClass(object):

      def __init__(self arg):
	  self.arg = arg

      data = some_data

      def method(self, arg):
	  print(self.data, arg)
#+END_SRC

The methods and data of the class can be accessed using the =.= (dot) notation.

#+BEGIN_SRC python
  my_object = MyClass(arg)

  print(my_object.data)

  my_object.method(arg)
 #+END_SRC

** Inheritance
Classes can also inherit attributes from other classes. This can be declared by substituting name of the superclass for =object=. This grants access to all the methods and data of the superclass. Essentially, every object of =MySubClass= class will at once be an object of =MyClass=. In the below case even the constructor (=__init__=) is inherited.

#+BEGIN_SRC python
  class MySubClass(MyClass):
      pass

  my_object_2 = MySubClass(arg)

  my_object_2.method()
#+END_SRC

The situation is somewhat complicated by the fact that inherited methods can be overwritten and altered. In fact, there are three distinct ways an attribute of the same name as in the superclass can be used in our subclass:
+ implicit: this is when the attribute of the superclass is used verbatim
+ override: this is when the implementation of the attribute is completely different, but still has the same name
+ altered: this is when the attribute of the superclass is directly accessed, but is extended

This last option is achieved using the =super= keyword. In general, this allows any attribute of the superclass be used, even the ones that have been overridden in the subclass.

#+BEGIN_SRC python
  class MyOtherClass(object):
      def implicit(self):
	  print("PARENT implicit")

      def override(self):
	  print("PARENT override")

      def altered(self):
	  print("PARENT altered")

  class MyOtherSubClass(MyOtherClass):
      def override(self):
	  print("CHILD override")

      def altered(self):
	  print("CHILD altered BEFORE")
	  super(MyOtherSubClass, self).altered()
	  print("CHILD altered AFTER")

  my_other_object = MyOtherClass()
  my_other_object_sub = MyOtherSubClass()

  return my_other_object.implicit()
  return my_other_object_sub.implicit()
  return my_other_object.override()
  return my_other_object_sub.override()
  return my_other_object.altered()
  return my_other_object_sub.altered()
#+END_SRC

A class can inherit from multiple other classes, but this raises the complexity of the class hierarchy significantly, so it should be used sparingly. Instead, the use of *composition* is encouraged

** Composition
Classes can have other classes as data. This gives access to the interface of the other class.

#+BEGIN_SRC python
  class Something(object):
      some_data = "data1"

      def some_method():
	  pass

  class MyThirdClass(object):
      my_something = Something()

  my_third_object = MyThirdClass()

  return(my_third_object.my_something.some_data)
#+END_SRC

#+RESULTS:
: data1

** Overloading operators
Overloading operators is somewhat cumbersome in python. You can only overload those operators for which a special functions is provided. The basic mathematical operators are among these. Another special method is =__repr__= which is supposed to return a string, the text representation of the object.

In the following example a class is created that represents complex numbers in the form x+iy.
#+BEGIN_SRC python
  class Complex(object):
      def __init__(self, re, im):
	  self.__re = re
	  self.__im = im

      def get_re(self):
	  return self.__re

      def get_im(self):
	  return self.__im

      def __add__(self, other):
	 sum_re = self.__re + other.get_re()
	 sum_im = self.__im + other.get_im()
	 return Complex(sum_re, sum_im)

      def __mul__(self, other):
	 prod_re = self.__re * other.get_re() - self.__im * other.get_im()
	 prod_im = self.__re * other.get_im() + self.__im * other.get_re()
	 return Complex(prod_re, prod_im)

      def __repr__(self):
	 return str(self.__re) + '+' + str(self.__im) + "i"

      def __eq__(self, other):
	 return self.__re == other.get_re() and self.__im == other.get_im()


  z1 = Complex(2, 1)
  z2 = Complex(3, 4)

  return(z1 + z2, z1 * z2, z1 == z2)
#+END_SRC

#+RESULTS:
| 5+5i | 2+11i | False |

* Functional Features in Python
** Higher-Order Functions & Lambdas
In python everything is an object, so in particular so are function. This means that functions can be passed as arguments to other functions and can be returned by them too.

#+BEGIN_SRC python
  def fmap_list(f, my_list):
      new_list = []
      for element in my_list:
	  new_list.append(f(element))
      return new_list

  def add_one(number):
      return number + 1

  my_list = [0, 1, 2, 3]

  return fmap(add_one, my_list)
#+END_SRC

#+RESULTS:
| 1 | 2 | 3 | 4 |

Anonymous functions can also be defined using the =lambda= keyword.

#+BEGIN_SRC python
  def adder(n, m):
      return n + m

  def partial_adder(n):
      return lambda m: adder(n, m)

  return partial_adder(3)(4)
#+END_SRC

#+RESULTS:
: 7

** Iterators and Generators
Iterators are objects over which we can loop. Whenever a =for X in Y= statement is used, =Y= is an iterator. Even in the case of lists. Then an implicit conversion is happening. It can be made explicit using the =iter()= function.

The following two are equivalent.
#+BEGIN_SRC python
  my_list = [1, 2, 3, 4]
  for element in my_list:
      pass

  for element in iter(my_list):
      pass
#+END_SRC

An object beomes an iterator if the =__next__()= method is implemented. This should return the next "element" of the object. Once out of elements, =__next__()= should raise a =StopIteration= exception.

The following built-in types support iterators: =list=, =dictionary=, =tuple=, =string=, =file=.

If iterators are objects that provide looping functionality, then /generators/ are the ones providing the data over which to iterate. The difference between objects "classic" that can be looped over, like lists, and generators is that in order to be looped over, a list has to be completely stored in memory, while generators provide elements /on-demand/. This lets us work with large datasets, for example.

Seen from a different angle, generators are like functions that have the ability to resume execution even after returning a value. This can be realised using the =yield= keyword. See the following two examples.
#+BEGIN_SRC python
  def simplest_generator():
      yield 1
      yield 2
      yield 3

  x = simplest_generator()
  
  return x.__next__(), x.__next__(),  x.__next__()
#+END_SRC

#+RESULTS:
| 1 | 2 | 3 |

#+BEGIN_SRC python
  def simple_generator(n):
      for i in range(n):
	  yield i

  x = simple_generator(3)
  return x.__next__(), x.__next__(),  x.__next__()
#+END_SRC

#+RESULTS:
| 0 | 1 | 2 |

** Built-in Functions
We now take a look at the holy trinity of "functional style" functions: =map=, =filter= and =reduce=. The former two are included in basic python, while the latter has been exiled into the =functools= module.

Both =map= and =filter= return iterator objects that can be cast to lits or sets or other types.
#+BEGIN_SRC python
  my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8]

  x = map((lambda x: x**2), my_list)
  y = filter((lambda x: x % 2 == 0), my_list)

  return type(x), list(x), type(y), list(y)
#+END_SRC

#+RESULTS:
| <class | map | > | (0 1 4 9 16 25 36 49 64) | <class | filter | > | (0 2 4 6 8) |

The function =reduce= works a lot like Haskell's =fold= functions. It accumulates the elements of the iterator using a function.
#+BEGIN_SRC python
  from functools import reduce

  my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8]
  x  = reduce((lambda x, y: x + y), my_list)

  return x
#+END_SRC

#+RESULTS:
: 36

** Typing
I know that this is not strictly 'functional', but I am coming from =Haskell=. Even the syntax is somewhat similar.

It is possible to explicitly specify types in python, even though the python runtime will NOT enforce them. This makes it mostly superfluous, unless for the fact that they might be useful for other programmers.
#+BEGIN_SRC python
  def greet(name: str) -> str:
      return "Greetings, Sir " + str(name)

  return greet(True)
#+END_SRC

#+RESULTS:
: Greetings, Sir True

* Data Structures
** Lists
Python supports list comprehension
#+BEGIN_SRC python
  my_list = [2*x-3 for x in range(2, 10)]

  return my_list
#+END_SRC

#+RESULTS:
| 1 | 3 | 5 | 7 | 9 | 11 | 13 | 15 |

*** =range= and slices

The =range()= function takes three argument (=start=, =stop=, =step=) and returns a sequenxe of integers. The first element of the sequence will be =start=, the last one =stop-1=, and step specifies the difference between neighbouring elements.

#+BEGIN_SRC python
  my_range = range(2, 30, 4)
  my_list = [x for x in my_range]
  
  return my_list
#+END_SRC

#+RESULTS:
| 2 | 6 | 10 | 14 | 18 | 22 | 26 |

We can also take out 'slices' of lists.
#+BEGIN_SRC python
  my_list = [1, 2, 3, 4, 5, 6, 7, 8]
  my_slice = my_list[2:5]

  return my_slice
#+END_SRC

#+RESULTS:
| 3 | 4 | 5 |

This lets us copy lists as well.
#+BEGIN_SRC python
  my_list = [1, 2, 3, 4]
  my_copy = my_list[:]
  my_list.pop()

  return (my_list, my_copy)
#+END_SRC

#+RESULTS:
| 1 | 2 | 3 |   |
| 1 | 2 | 3 | 4 |

*** Useful list methods
+ =append(x)=: append =x= to the end of the list
+ =pop()=: remove and return the last element of the list
+ =pop(n)= remove and return the element of index =n=
+ =sort()=
+ =reverse()=
+ =len()=
+ =insert(i, x)=: insert element =x= at index =i=
+ =count(x)=: returns how many duplicates of =x= there are in the list
** Tuples
Tuples are very similar to lists with the important difference that they are *immutable*. Once defined, its elements cannot be modified or deleted, or in any way manipulated. They can be constructed with parentheses. The =len()= method might be used on tuples.

#+BEGIN_SRC python
  my_tuple = ("tree", 12, False, 21.0)

  return (my_tuple[0], len(my_tuple)) # Here a tuple is returned
#+END_SRC

#+RESULTS:
| tree | 4 |

** Dictionaries
Lits are indexed by natural numbers. Dictionaries can be indexed by almost anything. They can be used to create association between pairs of values. They can be constructed using curly braces. Values can be accessed and modified just like in the case of lists.
#+BEGIN_SRC python
  my_dict = {"name": "Haskell Curry", "year of birth": 1900, True: "Maybe not...", 42: False}

  my_dict[42] = True
  return my_dict[42]
#+END_SRC

#+RESULTS:
: True

#+BEGIN_COMMENT
NB: There is a method for dictionaries called =get()=. It does almost the same as the standard =[]= accessors, but you can also specify a default value in case the key is missing.
#+END_COMMENT

Looping through a dictionary can be done multiple ways. First, using the =keys()= method which returns a list of the keys of the dictionary. Actually, it turns out that this is the default behavior, so =keys()= does not have to be used expicitly.
#+BEGIN_SRC python
    my_dict = {"name": "Haskell Curry", "year of birth": 1900, True: "Maybe not...", 42: False}

    my_list = []
    for key in my_dict:
	my_list.append(my_dict[key])

    return my_list
#+END_SRC

#+RESULTS:
| Haskell Curry | 1900 | Maybe not... | False |

Note that you can iterate over the values of the dictionary directly using the =values()= method.

Another option is to use the =items()= method which returns a list of =(key, value)= tuples allowing the following looping mechanism.
#+BEGIN_SRC python
  my_dict = {"name": "Haskell Curry", "year of birth": 1900, True: "Maybe not...", 42: False}

  my_string = ""
  for key, value in my_dict.items():
      my_string += f"The value associated with '{key}' is '{value}'.\n"

  return my_string
#+END_SRC

#+RESULTS:
: The value associated with 'name' is 'Haskell Curry'.
: The value associated with 'year of birth' is '1900'.
: The value associated with 'True' is 'Maybe not...'.
: The value associated with '42' is 'False'.

Dictionaries are ordered and duplicates amongst the keys are not allowed. The =len()= function may be used.

** Sets
Sets are unordered lists without duplicates. They can be constructed with curly braces. Elements inside a set cannot be changed, but elements can be added and removed. (This makes sense, since sets are unordered, so we have no way of specifying /which/ element should be changed.)

#+BEGIN_SRC python
  my_set = {"tree", 42, False, 'f', 1, 2, 3, 4}
#+END_SRC

We can loop through sets, and the order of the looping will be the sorted order of the elements. (As if it was a sorted list.) The =len()= functin may be used.

Note that in order to create an empty set, the =set()= function should be used, as ={}= creates an empty dictionary.

Set comprehensions are also supported
#+BEGIN_SRC python
  my_set = { 2*x-3 for x in range(2, 6)}

  return my_set
#+END_SRC

#+RESULTS:
| 1 | 3 | 5 | 7 |

** Trees
Even though the =binarytree= module is built-in, we are going to give a simple implementation of a binary search trees.
#+BEGIN_SRC python
  class Node(object):
      def __init__(self, value, left=None, right=None):
	  self.value = value
	  self.left = left
	  self.right = right

      def insert(self, value):
	  if self.value == value:
	      break
	  elif value < self.value:
	      if self.left is None:
		  self.left = Node(value)
	      else:
		  self.left.insert(value)
	  else:
	      if self.right is None:
		  self.right = Node(value)
	      else:
		  self.right.insert(value)

		  
  def from_list(list):
      if list == []:
	  return None
      else:
	  my_tree = Node(list[0])
	  for element in list[1:]:
	      my_tree.insert(element)
	  return my_tree

  def to_list(tree):
      if tree is None:
	  return []
      else:
	  return to_list(tree.left) + [tree.value] + to_list(tree.right)

  # I couldn't resist...
  def fmap_tree(f, tree):
      if tree is None:
	  return None
      else:
	  return Node(f(tree.value), fmap_tree(f, tree.left), fmap_tree(f, tree.right))

  my_list = [1, 5, 3, 8, 6, 9, 100]
  my_sorted_list = to_list(from_list(my_list))

  return my_sorted_list
#+END_SRC

#+RESULTS:
| 1 | 3 | 5 | 6 | 8 | 9 | 100 |

** Heaps
(Min)Heaps are essentially full/gapless binary trees in which every child is bigger then its parent. Since it is gapless, it can be stored as a list, and the tree structure can still be recovered.
#+BEGIN_SRC python
  def build_heap(list):
    heap = []
    for element in list:
	insert_min_heap(heap, element)

	return heap
#+END_SRC

*** Insert into (min) heap
#+BEGIN_SRC python
def get_parent(heap, index):
    if index == 0:
        raise IndexError("This is the root, it has no parent")
    elif index % 2 == 0:
        parent_index = (index - 2) // 2
        return parent_index, heap[parent_index]
    elif index % 2 == 1:
        parent_index = (index - 1) // 2
        return parent_index, heap[parent_index]

def insert_min_heap(heap, element):
    index = len(heap)
    heap.append(element)

    while True:
        try:
            parent_index, parent_value = get_parent(heap, index)
            if element < parent_value:
                heap[index], heap[parent_index] = parent_value, element
                index = parent_index
            else:
                break
        except IndexError:
            break
#+END_SRC

*** Delete from (min) heap
#+BEGIN_SRC python
  def get_left_child(heap, index):
      left_index = 2 * index + 1
      return left_index, heap[left_index]

  def get_right_child(heap, index):
      right_index = 2 * index + 2
      return right_index, heap[right_index]

  def delete_min_heap(heap, index):
      replacement = heap[-1]
      heap[index] = replacement
      heap.pop()

      while True:
	  try:

	      # We want to swap with the smaller of the children
	      left_index, left_value = get_left_child(heap, index)
	      right_index, right_value = get_right_child(heap, index)
	      if left_value < right_value:
		  child_index, child_value = left_index, left_value
	      else:
		  child_index, child_value = right_index, right_value

	      # Swapping...
	      if child_value < replacement:
		  heap[index], heap[child_index] = child_value, replacement
		  index = child_index
	      else:
		  break

	  except IndexError:
	      break
#+END_SRC

** Trie
How to insert a word into a trie?
1. Take the first letter and the trie.
2. Check is there is a node with the same letter in trie.children
3. If there is, then there is nothing to do, move on to the next letter
4. If there isn not, then create a node with it and append it to the list of children
5. Take the next letter and the trie starting at the node that holds the previous letter
   and go to point 2.
6. If there are no more letters, return the new trie
#+BEGIN_SRC python
  class TrieNode(object):
      def __init__(self, value, children=None):
	  self.val = value
	  self.children = children

      def get_children_value(self):
	  children_value = []
	  try:
	      for child in self.children:
		  children_value.append(child.val)

	      return children_value

	  except TypeError:
	      return []

  def trie_insert(trie, word):
      word += '\0'
      current_node = trie
      for letter in word:

	  try:
	      index = current_node.get_children_value().index(letter)
	      current_node = current_node.children[index]
	  except ValueError:
	      try:
		  current_node.children.append(TrieNode(letter))
		  current_node = current_node.children[-1]
	      except AttributeError:
		  current_node.children = [TrieNode(letter)]
		  current_node = current_node.children[0]


  def build_trie(words):
      trie = TrieNode('#') # This is the root of the tree
      for word in words:
	  trie_insert(trie, word)

      return trie

  def print_trie(trie):
      children = ""
      try:
	  for child in trie.children:
	      children += print_trie(child)
      except TypeError:
	  pass

      return f"{trie.val}: [{children}]"

  def trie_search(trie, word):
      current_node = trie

      for letter in word:
	  try:
	      index = current_node.get_children_value().index(letter)
	      current_node = current_node.children[index]

	  except ValueError:
	      return False

      return True

  words = "kés villa villő világ villám".split()
  trie = build_trie(words)
  return print_trie(trie)
#+END_SRC

Tries can be used to match prefixes.
#+BEGIN_SRC python
  def find_all_words(trie):

      def scan(trie, words, string):
	  for child in trie.children:
	      modified_string = string[:]
	      if child.val == '\0':
		  words.append(string)
	      else:
		  modified_string += child.val
		  words = scan(child, words, modified_string)

	  return words

      return scan(trie, [], "")


  def with_prefix(trie, prefix):

      def last_common_node(trie, prefix):
	  current_node = trie
	  for letter in prefix:
	      try:
		  index = current_node.get_children_value().index(letter)
		  current_node = current_node.children[index]
	      except ValueError:
		  return None

	  return current_node

      lcn = last_common_node(trie, prefix)
      suffixes = find_all_words(lcn)

      words_with_prefix = []

      for suffix in suffixes:
	  words_with_prefix.append(prefix + suffix)

      return words_with_prefix
#+END_SRC

** Suffix Tree
Suffix trees are compressed tries where consequent nodes that have only 1 children are merged. It is built out of all suffixes in a griven text.
#+BEGIN_SRC
                  root                     root
                  /  \                     /  \
                 k    v                  kés  vil
                 |    |                      /   \
                 é    i                     l    ág
                 |    |       ==>         / | \
                 s    l                  a  ő ám
                    /   \
                   l     á
                 / | \   |
                a  ő  á  g
                      |
                      m
#+END_SRC

Compressing a trie consists of the following steps:
1. Take a TrieNode and check how many children it has
2. If none, then do nothing
3. If only one, then compress the child and add its value to its parent, and reassign the child's children to the parent
4. If more then one, then loop through them and run compress on each

  #+BEGIN_SRC python
  def compress_trie(trie):
      try: 
	  number_children = len(trie.children)
      except TypeError:
	  number_children = 0

      if number_children == 0:
	  return None
    
      elif number_children == 1:
	  child = trie.children[0]
	  compress_trie(child)
	  trie.val += child.val
	  trie.children = child.children
	  return None
    
      else:
	  for child in trie.children:
	      compress_trie(child)
	  return None
#+END_SRC

The next step is to generate all suffixes in a given text.

#+BEGIN_SRC python
  def all_suffixes(string):
      suffixes = []
      length = len(string)

      for left in range(length - 1):
	for right in range(left, length):
	  suffixes.append(string[left:right])

      return suffixes


  return all_suffixes("banana")
#+END_SRC

Now we can build our suffix tree.

#+BEGIN_SRC python
  def build_suffix_tree(string):
      suffixes = all_suffixes(string)

      suffix_tree = build_trie(suffixes)

      compress_trie(suffix_tree)

      return suffix_tree
#+END_SRC

** Graphs
We are going to model directed graphs with dictionaries that have nodes as keys, and lists of outgoing edges (represented by their end node) as values.
#+BEGIN_SRC python
  class Graph(object):
      def __init__(self, gdict=None):
	  if gdict in None:
	      gdict = {}
	  self.gdict = gdict

  graph_elements = {'a':['b', 'c'],
		    'b':['a', 'd'],
		    'c':['a', 'd'],
		    'd':['e'],
		    'e':['d']}

  graph = Graph(graph_elements)
#+END_SRC

*** BFS
Breadth-first search is an algorithm aiming to traverse a graph by first visiting all neighbours of a node before moving on to the 'next level'.
#+BEGIN_SRC python
    def bfs(graph, start):

	discovered = {start: (0, None)}
	visited = []
	queue = [start]

	while queue:
	    current_node = queue.pop(0)

	    for neighbour in graph.gdict[current_node]:
		if neighbour not in discovered:
		    discovered[neighbour] = (discovered[current_node][0] + 1, current_node)
		    queue.append(neighbour)

	    visited.append(current_node)

	return discovered
#+END_SRC

*** DFS
Depth-first search is a graph traversing algorithm that
#+BEGIN_SRC python
def dfs(graph, start):

    def run_dfs(graph, start, map, counter):
        map[start] = [counter, None]
        counter += 1

        for neighbour in graph.gdict[start]:
            if neighbour not in map:
                map, counter = run_dfs(graph, neighbour, map, counter)

        map[start][1] = counter
        counter += 1

        return map, counter

    map, _ = run_dfs(graph, start, {}, 1)

    return map
#+END_SRC
** Hash Tables
* Algorithms
** Binary Search
Binary search can be used for searching in sorted arrays
#+BEGIN_SRC python
  def bin_search(array, element):
      print(array)
      index, middle = list(enumerate(array))[len(array) // 2]
      if element == middle:
	  return index
      elif element < middle:
	  return bin_search(array[:index], element)
      elif element > middle:
	  return bin_search(array[index:], element)
      else:
	  raise ValueError("The required element is not in the list")


  my_array = [1, 3, 5, 7, 9, 11, 13]
  return bin_search(my_array, 7)
#+END_SRC

** Sorting Algorithms
*** merge sort
#+BEGIN_SRC python
  def merge_sorted(l1, l2):
      try:
	  e1 = l1.pop(0)
      except IndexError:
	  return l2

      try:
	  e2 = l2.pop(0)
      except IndexError:
	  return l1

      sorted = []

      while True:
	  if e1 < e2:
	      sorted.append(e1)
	      try:
		  e1 = l1.pop(0)
	      except IndexError:
		  sorted.append(e2)
		  sorted += l2
		  return sorted
	  else:
	      sorted.append(e2)
	      try:
		  e2 = l2.pop(0)
	      except IndexError:
		  sorted.append(e1)
		  sorted += l1
		  return sorted


  def mergesort(l):
      if len(l) <= 1:
	  return l
      else:
	  length = len(l) // 2
	  left = mergesort(l[:length])
	  right = mergesort(l[length:])
	  return merge_sorted(left, right)


  return mergesort([1, 4, 2, 3, 9, 5, 8, 7, 6])

#+END_SRC

#+RESULTS:
| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |

*** quicksort
#+BEGIN_SRC python
    def quicksort(l):
	length = len(l)
	if length < 2:
	    return l

	else:
	      pivot = l[-1]
	      left = 0
	      right = length - 2
	      while left < right:
		  if l[left] > pivot and l[right] < pivot:
		      l[left], l[right] = l[right], l[left]
		  if l[left] < pivot:
		      left += 1
		  if l[right] > pivot:
		      right -= 1


	     half = length // 2
	     smaller = l[:half]
	     bigger = l[half:]

	       return quicksort(smaller) + quicksort(bigger)

      return quicksort([1, 4, 2, 3, 9, 5, 8, 7, 6])

#+END_SRC

#+RESULTS:

*** Dynamic Programming
* Modules & Packages
In essence, a module is a =.py= file and a package is a folder with a =__init__.py= file in it. The latter file might even be empty.

The class =MyClass= can be imported from the file =my_module.py= (or from the folder =my_module= with an =__init__.py= file) the following way.

#+BEGIN_SRC python
  from my_module import MyClass
#+END_SRC

No special syntax is needed in the =my_module.py= file.
* Error and Exception Handling
There are 3 "types" of errors in python:
+ =SyntaxError=: when an expression does not conform to the rules of python grammar
+ built-in exceptions
+ user-defined exceptions (we will get back to these later

** The =try...except= statement

Is a =SyntaxError= is raised there is nothing one can do other than correcting the syntax. If, however, one of the other two types of exceptions is raised, certain mitigations are possible. These mitigations are called *exception handling* and are achieved using =try= statements.

#+BEGIN_SRC python
  while True:
      try:
	  x = int(input("> "))
	  break
      except ValueError:
	  print("That is not a number...")
#+END_SRC

The following happens here: First the =try= statement is executed. If it executes without error, the =except= part is skipped. If, however, an error is raised, and is the same type of error as specified in the =except= clause, then the block under the =except= clause is executed. If the exception raised is not the same as the one in the =except= expression then the exception is passed on to the next =except= statement. If there is no such statement, then we have an /unhandled exception/ and execution stops with an error.

NB: There can be more than one =except= block under a single =try= block for different types of exceptions.

There is an optional =else= cluse which is executed if the =try= clause does not raise an exception.

** Raising Exceptions
Exceptions can be raised manually using the =raise= keyword.

#+BEGIN_SRC python

  number = int(input("Type a number between 0 and 10: ")
  if number < 0 or number > 10:
      raise ValueError("This is not between 0 and 10")
#+END_SRC

** User-defined Exceptions

By creating classes that inherit from the =Exception= class, custom exceptions can be defined.
#+BEGIN_SRC python
  class MyError(Exception):
      pass

  if True:
      raise MyError("There is nothing you can do...")
#+END_SRC

* Project Management
A generic python repository should look like the following.
#+BEGIN_SRC
  README.md
  LICENSE
  setup.py
  requirements.txt
  sample/
  docs/
  tests/
#+END_SRC

"Sample" is the name of the program, so the folder =sample= contains the actual source code. If the module is a single file, the directory can be replaced with the =sample.py= file.

The =setup.py= file should containt all the information about package and distribution management. This is a sample =setup.py= file
#+NAME: setup.py
#+BEGIN_SRC python
  from setuptools import setup

  with open('requirements.txt') as f:
      install_requires = f.read().splitlines()

  setup(
    name='sample',
    #packages=['someprogram'],
    version='0.1.0',
    #author='...',
    #description='...',
    install_requires=install_requires,
    scripts=[
      'someprogram.py',
    ],
    entry_points={
      # example: file some_module.py -> function main
      #'console_scripts': ['someprogram=some_module:main']
    },
  )
#+END_SRC

The =requirements.txt= file should contain all dependencies required to contribute to the project.
* Automated Testing with =unittest=

The built-in =unittest= module lets us automate testing. The tests have to be put into a class that inherits from =unittest.TestCase=, so that the assertion methods can be used.

The following methods are avaliable:
| method                   | equivalent to      |
|--------------------------+--------------------|
| =assertEqual(a, b)=      | =a == b=           |
| =assertIs(a, b)=         | =a is b=           |
| =assertTrue(a)=          | =bool(a) is True=  |
| =assertFalse(a)=         | =bool(a) is False= |
| =assertIsNone(a)=        | =a in None=        |
| =assertIn(a, b)=         | =a in b=           |
| =assertIsInstance(a, b)= | =isinstance(a, b)= |

There is also =assertRaises(error, callable, args)= that checks is calling =callable= with the arguments =args= will result in exception =error= being raised.

#+NAME: tests_module_to_test.py
#+BEGIN_SRC python
  import unittest
  import module_to_test

  class MyTests(unittest.TestCase):
      def test_1(self):
	  my_sum = sum([1, 2, 3])
	  self.assertEqual(my_sum, 6)

      def test_2(self):
	  self.assertRaises(ZeroDivisionError, this_raises_error, 2)


  def this_raises_error(n):
    return n / 0

  if __name__ == "__main__":
	unittest.main()
#+END_SRC

NB: All testing function defined inside the class have to start with =test=.

* Miscellaneous Features
** Decorators
Decorators allow us to create general wrapper functions that extend the functionality of multiple functions. The syntax for it is demonstrated below.
#+BEGIN_SRC python
  def decorator(f):
      def wrapper(*args, **kwargs):
	  print("Start")
	  f(*args, **kwargs)
	  print("Finish")
      return wrapper

  @decorator
  def func(string):
      print(string)

  return func("valami") # now this is equivalent to wrapper(fun)
#+END_SRC

We can use this to create, e.g., a =timer= decorator that times a function. This means that we have only to write the timing-related parts once, and can use it to decorate, and thus time, many other function. Another use case would be checking input multiple times.

#+BEGIN_SRC python
  import time

  def timer(f):
      def wrapper(*args, **kwargs):
	  start_time = time.perf_counter()
	  return_values = f(*args, **kwargs)
	  print(time.perf_counter() - start_time)
	  return return_values
      return wrapper

  @timer
  def sleeping():
      time.sleep(2)

  sleeping()
#+END_SRC

** Context Managers
Context managers are similar to decorators in that they enhance the usability of some functions. The prototypical example of context managers is opening files. Here the context is /a file being open/. Whatever happens during the time the file is open, one thing is for sure: it has to be closed, even if the program crashes halfway through. A context manager lets us define specific actions to perform once we enter and exit the context.

The aforementioned file-opening context is built into python...
#+BEGIN_SRC python
  with open("filename.txt", 'r') as file:
      print(file.read())
#+END_SRC

... but we can reimplement it for example's sake.
#+BEGIN_SRC python
  class File(object):
      def __init__(self, filename, mode):
	  self.file = open(filename, mode)

      def __enter__(self):
	  pass

      def __exit__(self, type, value, traceback):
	  self.file.close()

  with File("filename.txt", 'r') as file:
      print(file.read())

#+END_SRC

The important things here are the two special methods, =__enter__= and =__exit__=. These are called once we enter and exit the context, respectively.

As can be seen, there are a bunch of extra arguments to the =__exit__= method. These have to do with error handling, and are passed to the method by the interpreter. We can use them to catch exceptions:
#+BEGIN_SRC python
class File(object):
    def __init__(self, filename, mode):
        self.filename = filename
        try:
            self.file = open(filename, mode)
        except FileNotFoundError:
            with open(filename, 'w') as new_file:
                print("This file does not exist, creating it now...")
                new_file.write("This file did not exist, created it just now.\n")
            self.file = open(filename, mode)

    def __enter__(self):
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()
        if exc_type is Exception:
            print("I don't know what this is...")
            print("But let's continue, I guess...")
            return True

with File("valami.txt", 'r') as my_file:
    return my_file.readline()
    raise Exception
	  #+END_SRC

#+RESULTS:
: I have just created this file

The =return True= part makes sure that the execution resumes.
** Logging
Python also provides an advenced =logging= module that allows the fune tuning of logs.
Using the =basicConfig= function we can specify a bunch of things about the logging process:
+ =level=: The module differentiates 5 levels of logging (see in the code example). This variable specifies the lowest priority of logs that will show up
+ =filename=: The name of the file where the logging shall go
+ =filemode=: Just like in the case of opening files
+ =format=: This is just a format string for the log messages

The =logging.exception= function also returns the stack traceback of the exception. The same effect can be achieved by passing =exc_info=True= to the logger.
#+BEGIN_SRC python
  import logging as l

  # This should only be used once, before any logging
  l.basicConfig(level=l.INFO, filename="my_log.log", filemode='w',
		format="%(asctime)s - %(levelname)s - %(message)s")

  l.debug("debug")
  l.info("info")
  l.warning("warning")
  l.error("error")
  l.critical("critical")

  x = 42
  l.info(f"The value of x is {x}")

  try:
      1 / 0
  except ZeroDivisionError:
      l.exception("oh-oh")
#+END_SRC

These are the contents of the generated log file.

#+NAME: my_log.log
#+BEGIN_SRC 
2022-09-29 20:02:01,090 - INFO - info
2022-09-29 20:02:01,090 - WARNING - warning
2022-09-29 20:02:01,090 - ERROR - error
2022-09-29 20:02:01,090 - CRITICAL - critical
2022-09-29 20:02:01,090 - INFO - The value of x is 42
2022-09-29 20:02:01,090 - ERROR - oh-oh
Traceback (most recent call last):
  File "<stdin>", line 19, in main
ZeroDivisionError: division by zero
#+END_SRC

def backpack_problem(total, weights, values):
    table = [[None] * total for i in range(len(weights))]

    for index in range(len(weights)):

        for limit in range(total):

            if limit == 0 or index == 0:
                table[index][limit] = 0

            else:
                val1 = table[index - 1][limit]
                
                if limit - weights[index] < 0:
                    val2 = 0
                else:
                    val2 = table[index - 1][limit - weights[index]]

                val2 = val2 + values[index]
                
                table[index][limit] = max(val1, val2)

    return table[len(weights) - 1][total - 1]

print("backpack: ")
print(backpack_problem(20, [2, 5, 11, 3, 7], [4, 1, 5, 1, 9]))

def letra(nr):
    l = len(nr)
    climb = [False] * l

    climb[0] = True
    climb[1] = nr[1]
    climb[2] = nr[2]

    for index in range(3,l):
        if nr[index] and (climb[index - 1] or climb[index - 2] or climb[index - 3]):
            climb[index] = True

    print(climb)
    return climb[-1]

print(letra([True, True, True, False, False, False, True, False, True]))
            
def letra_szam(nr):
    l = len(nr)
    climb = [0] * l

    climb[0] = 1
    if nr[1]:
        climb[1] = 2

    if nr[2]:
        climb[2] = 3

    for index in range(3, l):
        if nr[index]:
            climb[index] = climb[index - 1] + climb[index - 2] + climb[index - 3]

    return climb[-1]


print(letra_szam([True, True, True, False, True, False, True, False, True]))


def lcss(str1, str2):

    m = len(str1)
    n = len(str2)

    table = [[0] * n for i in range(m)]

    for i in range(m):
        for j in range(n):
            if i == 0 or j == 0:
                if str1[i] == str2[j]:
                    table[i][j] = 1
                else:
                    table[i][j] = 0

            else:
                if str1[i] == str2[j]:
                    table[i][j] = table[i - 1][j - 1] + 1
                else:
                    table[i][j] = 0

    maximum = 0
    max_index = (0, 0)

    for i, line in enumerate(table):
        for j, entry in enumerate(line):
            if entry > maximum:
                maximum = entry
                max_index = (i, j)

    i, j = max_index
    print(i,j)
    return str1[i - maximum + 1:i + 1]


print(lcss("kisbélab", "bélabácsi"))

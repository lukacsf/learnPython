# I found this set of exercises in this git repo:
# https://github.com/learnbyexample/Python_Basics/blob/master/Exercises.md

def check(f, checks):
    for test in checks:
        print(f"input {test} gives output {f(test)}")

# Exercise 1
def exercise1():
    print("Please provide the following details.")
    name = input("Enter your name: ")
    department = input("Enter your department: ")
    college = input(" Enter your college: ")

    print("-----------------------------------")
    print(f"Name:        {name}")
    print(f"Departement: {department}")
    print(f"College:     {college}")

# exercise1()

# Exercise 2a
def len_int(num):
    if isinstance(num, int):
        return len(str(num))
    else:
        raise TypeError("provide only integer input")

checks2a = [9186391918273, 1, 32, "31"]

# check(len_int, checks2a)

# Exercise 2b
def str_cmp(str1, str2):
    return str1.lower() == str2.lower()

checks2b = [("nice", "nice"), ("Hi there", "hi there"), ("go0d DaY", "gOOd day")]

# for str1, str2 in checks2b:
#     print(str_cmp(str1, str2))

# Exercise 2c
def str_anagram(str1, str2):
    return set(str1.lower()) == set(str2.lower())

checks2c = [("God", "dog"), ("beat", "table"), ("Tap", "paT"), ("beat", "abet")]
# for str1, str2, in checks2c:
#     print(str_anagram(str1, str2))

# Exercise 2d
def num(n):
    if isinstance(n, int) or isinstance(n, float):
        return n
    elif isinstance(n, str):
        try:
            return int(n)
        except ValueError:
            try:
                return float(n)
            except ValueError:
                raise ValueError("This string does not represent a number")
    else:
        raise TypeError("Wrong argument type")

checks2d = [2, 0x1f, 3.32, "-78", "  42  \n  ", "3.982e5"]

# check(num, checks2d)

# Exercise 3a
def six_by_seven(num):
    if num % 7 == 0:
        if num % 6 == 0:
            return "Universe"
        else:
            return "Good"
    elif num % 6 == 0:
        return "Food"
    else:
        return "Oops"

# print(list(enumerate((six_by_seven(n) for n in range(100)))))

# Exercise 3b
def decimal_palindrome(num):
    string_num = str(num)
    return string_num == string_num[::-1]

def binary_palindrome(num):
    string_bin_num = bin(num)[2:]
    return string_bin_num == string_bin_num[::-1]

def exercise3b():
    for i in range(1000):
        if decimal_palindrome(i) and binary_palindrome(i):
            print(i, bin(i)[2:])
        else:
            pass

# exercise3b()

# Exercise 4a

import functools

def product(my_list):
    return functools.reduce((lambda x, y: x * y), my_list)

def exercise4a():
    print(product([1, 4, 21]))
    print(product([-4, 2.3e12, 77.23, 982, 0b101]))
    print(product({8, 300}))

# exercise4a()

# Exercise 4b
def nth_lowest(nums, n=1):
    no_dups = list(set(nums))
    no_dups_sorted = sorted(no_dups)
    return no_dups_sorted[n-1]

checks4b = [([1, -2, 4, 2, 1, 3, 3, 5], 4),
            ('unrecognizable', 3),
            ('abracadabra', 4)]
# for nums, index in checks4b:
#     print(nth_lowest(nums, index))
          
# Exercise 4c
def word_slices(word):
    l = len(word)
    indices = filter((lambda tup: tup[1] - tup[0] > 1), [(i, j) for i in range(l) for j in range(l+1)])
    slices = map((lambda tup: word[tup[0]:tup[1]]), indices)
    return list(slices)

# print(word_slices("table"))

# Exercise 5a

with open("f1.txt", 'r') as f:
    nums = []
    for line in f:
        try:
            number = int(line)
        except ValueError:
            number = float(line)
        nums.append(number)

# print(sum(nums))

# Exercise 5b
def check_next_char(word, index):
    try:
        next_char = word[index + 1]
        int(next_char)
        return True
    
    except ValueError:
        return False
    
    except IndexError:
        return False

def parse_int(word):
    numbers = []
    number = ""
    
    for index, letter in enumerate(word):
        
        try:
            int(letter)
            number += letter
            
            if check_next_char(word, index):
                pass
            
            else:
                numbers.append(int(number))
                number = ""
                
        except ValueError:
            pass
        
    return numbers

def extract_sum():
    with open("f2.txt", 'r') as f:
        nums = []
        
        for line in f:
            for word in line.split():
                numbers = parse_int(word)
                nums += numbers

    return sum(nums)

# print(extract_sum())

# Exercise 5c
with open("f3.txt", 'r') as f:
    split_files = map((lambda s: s.strip().split('.')), f)
    sorted_files = sorted(split_files, key=lambda l: l[-1].lower())

# print(sorted_files)

# Exercise 6a
def is_one_char_diff(w1, w2):
    return sum(1 for _ in filter((lambda tup: tup[0] != tup[1]), zip(w1.lower(), w2.lower()))) == 1

# print(is_one_char_diff("baz", "Bar"))

# Exercise 6b
def is_alpha_order(word):
    word = word.lower()
    list_word = list(word)
    ascending = sorted(word) == list_word
    descending = sorted(word, reverse=True) == list_word
    return ascending or descending

def is_alpha_order_sentence(sentence):
    return all(map((lambda word: is_alpha_order(word)), sentence.split()))

# print(is_alpha_order_sentence("Toe got bit"))

# Exercise 6c
def max_nested_braces(string):
    # Create a stack to keep track of opened braces
    stack = []
    max_nesting = 0

    # loop over the string, push/pop braces and discard other chars-
    for char in string:

        # if the current character is an opening brace, push it to the stack
        if char == '{':
            stack.append(char)

            # update max_nesting
            if len(stack) > max_nesting:
                max_nesting = len(stack)

        # is the current character is a closing brace, pop it from the stack.
        # If epression is not well-formed, IndexError might be raised
        elif char == '}':
            try:
                stack.pop()
            except IndexError:
                return -1

        # Discard every other character
        else:
            pass
    # If stack is not empty at the end, the expression is not well-formed
    if stack == []:
        return max_nesting
    else:
        return -1

# print(max_nested_braces("{{a+2}*{{b+{c*d}}+e*d}}"))
# print(max_nested_braces('}a+b{'))

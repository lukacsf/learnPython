class TrieNode(object):
    def __init__(self, value, children=None):
        self.val = value
        self.children = children

    def get_children_value(self):
        children_value = []
        try:
            for child in self.children:
                children_value.append(child.val)

            return children_value
        
        except TypeError:
            return []

        
def trie_insert(trie, word):
    word += '$'
    current_node = trie
    for letter in word:

        try:
            index = current_node.get_children_value().index(letter)
            current_node = current_node.children[index]
        except ValueError:
            try:
                current_node.children.append(TrieNode(letter))
                current_node = current_node.children[-1]
            except AttributeError:
                current_node.children = [TrieNode(letter)]
                current_node = current_node.children[0]
            
        
def build_trie(words):
    trie = TrieNode('#')
    for word in words:
        trie_insert(trie, word)

    return trie


def print_trie(trie):
    children = ""
    try:
        for child in trie.children:
            children += print_trie(child)
    except TypeError:
        pass

    return f"{trie.val}: [{children}]"


def trie_search(trie, word):
    word += '$'
    current_node = trie

    for letter in word:
        try:
            index = current_node.get_children_value().index(letter)
            current_node = current_node.children[index]

        except ValueError:
            return False

    return True


def find_all_words(trie):
    
    def scan(trie, words, string):
        for child in trie.children:
            modified_string = string[:]
            child_value = child.val
            if child_value[-1] == '$':
                words.append(string + child_value[:-1])
            else:
                modified_string += child_value
                words = scan(child, words, modified_string)

        return words
            
    return scan(trie, [], "")
    

def with_prefix(trie, prefix):

    def last_common_node(trie, prefix):
        current_node = trie
        for letter in prefix:
            try:
                index = current_node.get_children_value().index(letter)
                current_node = current_node.children[index]
            except ValueError:
                return None

        return current_node

    lcn = last_common_node(trie, prefix)
    suffixes = find_all_words(lcn)

    words_with_prefix = []

    for suffix in suffixes:
        words_with_prefix.append(prefix + suffix)

    return words_with_prefix


def compress_trie(trie):
    def run_compression(trie):
        try: 
            number_children = len(trie.children)
        except TypeError:
            number_children = 0

        if number_children == 0:
            return None

        elif number_children == 1:
            child = trie.children[0]
            run_compression(child)
            trie.val += child.val
            trie.children = child.children
            return None

        else:
            for child in trie.children:
                run_compression(child)
            return None

    run_compression(trie)
    return trie


words = "kés villa villő világ villám".split()
trie = build_trie(words)
print(print_trie(trie))
com_trie = compress_trie(trie)


def all_suffixes(string):
    suffixes = []
    length = len(string)

    for left in range(length - 1):
      for right in range(left, length):
        suffixes.append(string[left:right])

    return suffixes


def build_suffix_tree(string):
    suffixes = all_suffixes(string)
    trie_of_suffixes = build_trie(suffixes)
    
    return compress_trie(trie_of_suffixes)


suftree = build_suffix_tree("xabxa")

print(print_trie(suftree))

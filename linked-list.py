class Node(object):
    def __init__(self, value=None, next_node=None):
        self.value = value
        self.next_node = next_node

class LinkedList(object):
    def __init__(self, head=None):
        self.head = head

    def append(self, value):
        if self.head is None:
            self.head = Node(value)
        else:
            current = self.head
            while current.next_node:
                current = current.next_node
            current.next_node = Node(value)
        return self

    def __iter__(self):
        return self

    def __next__(self):
        if self.head:
            x = self.head.value
            self.head = self.head.next_node
            return x
        else:
            raise StopIteration

    def map(self, f):
        current = self.head
        while current:
            current.value = f(current.value)
            current = current.next_node

    def __repr__(self):
        return "[[ " + self.__print_elements() + "]]"

    def __print_elements(self):
        if self.head is None:
            return ""
        else:
            return str(self.head.value) + ", " + LinkedList(self.head.next_node).__print_elements()

    def __add__(self, other):
        if other.head is None:
            return self
        else:
            head = other.head.value
            tail = LinkedList(other.head.next_node)
            return self.append(head).__add__(tail)

def len(ldl):
    if ldl.head is None:
        return 0
    else:
        tail = LinkedList(ldl.head.next_node)
        return 1 + len(tail)

def from_list(source_list):
    linked_list = LinkedList()
    for element in source_list:
        linked_list.append(element)
    return linked_list

my_linked_list = from_list([1, 2, 3, 4])
my_linked_list.map(lambda x: 2*x)

print(f"Test list: {my_linked_list}")

# And now.... how to reverse a linked list

# How to reverse a list?
# 1. If the list is empty, then its reverse is itself
# 2. If the list is nonempty, then
#    1. Take its head and tail
#    2. reverse its tail
#    3. append the head to the reversed tail.
def revldl_recursive(ldl):
    if ldl.head is None:
        return ldl
    else:
        head = ldl.head.value
        tail = LinkedList(ldl.head.next_node)
        rev_tail = revldl_recursive(tail)
        return rev_tail.append(head)

def revldl_recursive_compact(ldl):
    if ldl.head is None:
        return ldl
    else:
        return revldl_recursive_compact(LinkedList(ldl.head.next_node)).append(ldl.head.value)

my_ldl = from_list([1, 2, 3, 4])
print(revldl_recursive_compact(my_ldl))

def revldl_in_place(linked_list):
    # Set up what variables for the initial node
    # (We know explicitly that prev_node if None and curr_node is ldl.head)
    prev_node = None
    curr_node = linked_list.head
    next_node = curr_node.next_node

    # Do the reversing
    curr_node.next_node = prev_node

    # Check if whether we are at the end
    while next_node:

        # Set up variables for the current node
        prev_node = curr_node
        curr_node = next_node
        next_node = curr_node.next_node

        # Do the reversing
        curr_node.next_node = prev_node

    # Set that new head.
    linked_list.head = curr_node

revldl_in_place(my_ldl)
print(my_ldl)

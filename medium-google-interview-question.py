# I got this exercise from this YouTube video:
# https://www.youtube.com/watch?v=4tYoVx0QoN0

"""
So the idea is that it is a lot easier to filter out the dry land.

I can start from any (in fact, every) point of the border and
  I.  If it is 'water', then leave it alone
  II. If it is 'land', then
      1. Get all its 'land' neighbours
      2. Recursively erase them
      3. Erase the original

That way every land area connected to the initial border position
will be ceared. I done from every border position, eventually every
land area will be cleared and only the islands remain.
"""

class MyBitMap(object):

    # Initialise the object with the matrix and a
    # we are going to modify
    def __init__(self, matrix):
        self.matrix = matrix
        self.original = [row[:] for row in matrix]

    # Predicate whether 'position' is originally black
    def black_p(self, position):
        x, y = position
        return self.matrix[y][x]

    # Clean 'position' (make it 0)
    def clean(self, position):
        x, y = position
        self.matrix[y][x] = 0

    def __repr__(self):
        string = ""
        for row in self.matrix:
            string += str(row) + '\n'
        return string

    # returns all the positions that lie on the border
    def get_border(self):

        # The -1 is so that we get the range of possible coordinates
        len_y = len(self.matrix) - 1
        len_x = len(self.matrix[0]) - 1
        
        upper_border = [(x, 0) for x in range(len_x)]
        lower_border = [(x, len_y) for x  in range(len_x)]
        left_border = [(0, y) for y in range(len_y)]
        right_border = [(len_x, y) for y in range(len_y)]
        
        return upper_border + lower_border + left_border + right_border

    # returns all the neighbouring black positions
    def get_neighbourhood(self, position):
        x, y = position
        black_neighbours = []

        """
        Try every direction and test if there is a black
        neighbour there.
        """
        try:
            up_x, up_y = x, y + 1
            if self.matrix[up_y][up_x]:
                black_neighbours.append((up_x, up_y))
        except IndexError:
            pass
        
        try:
            down_x, down_y = x, y - 1
            if self.matrix[down_y][down_x]:
                black_neighbours.append((down_x, down_y))
        except IndexError:
            pass

        try:
            left_x, left_y = x - 1, y
            if self.matrix[left_y][left_x]:
                black_neighbours.append((left_x, left_y))
        except IndexError:
            pass

        try:
            right_x, right_y = x + 1, y
            if self.matrix[right_y][right_x]:
                black_neighbours.append((right_x, right_y))
        except IndexError:
            pass

        return black_neighbours

    # recursively removes black positions from the neighbourhood
    def clean_neighbours(self, position):
        black_neighbours = self.get_neighbourhood(position)
        self.clean(position)
        for neighbour in black_neighbours:
            self.clean(neighbour)
            self.clean_neighbours(neighbour)


    # recursively removes all black positions connected
    # to the initial border position
    def from_this_point(self, border_position):
        if self.black_p(border_position):
            self.clean_neighbours(border_position)
        else:
            pass

    # removes all black positions from the map
    def get_islands(self):
        border = self.get_border()
        for position in border:
            self.from_this_point(position)

    def remove_islands(self):
        self.get_islands()

        no_islands = []
        for rowO, rowI in zip(self.original, self.matrix):

            no_islands_row = []
            for entryO, entryI in zip(rowO, rowI):
                no_islands_row.append(entryO - entryI)

            no_islands.append(no_islands_row)

        for row in no_islands:
            print(row)


matrix1 = [[1, 0, 0, 0, 0, 0],
           [0, 1, 0, 1, 1, 1],
           [0, 0, 1, 0, 1, 0],
           [1, 1, 0, 0, 1, 0],
           [1, 0, 1, 1, 0, 0],
           [1, 0, 0, 0, 0, 1]]
test1 = MyBitMap(matrix1)

matrix2 = [[1, 0, 0],
           [0, 1, 0],
           [0, 0, 0]]
test2 = MyBitMap(matrix2)


test2.remove_islands()
